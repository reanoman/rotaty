﻿////////////////////////////////////////////////////////////////////////////////
//  
// @module Manifest Manager
// @author Alex Yaremenko (Stan's Assets) 
// @support support@stansassets.com
//
////////////////////////////////////////////////////////////////////////////////



using UnityEngine;
using System.Collections;

using SA.Foundation.Config;

namespace SA.Android.Manifest {

	public class AMM_Settings  {

        public const string PLUGIN_NAME = "Android Manifest";
        public const string VERSION_NUMBER = "2018.2.1";
        public const string MANIFEST_MANAGER_FOLDER = SA_Config.STANS_ASSETS_NATIVE_PLUGINS_PATH + "AndroidManifestManager/";

        public const string DEFAULT_MANIFEST_PATH =  MANIFEST_MANAGER_FOLDER + "Editor/Files/default.xml";

        public const string MANIFEST_FOLDER_PATH = SA_Config.UNITY_ANDROID_PLUGINS_PATH;
        public const string MANIFEST_FILE_PATH = MANIFEST_FOLDER_PATH + "AndroidManifest.xml"; 

	}
}