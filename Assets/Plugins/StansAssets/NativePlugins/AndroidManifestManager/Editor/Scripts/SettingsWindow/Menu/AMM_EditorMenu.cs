﻿////////////////////////////////////////////////////////////////////////////////
//  
// @module Manifest Manager
// @author Alex Yaremenko (Stan's Assets) 
// @support support@stansassets.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEditor;
using System.Collections;

using SA.Foundation.Config;


namespace SA.Android.Manifest {

    public static class AMM_EditorMenu {

        private const int PRIORITY = SA_Config.PRODUCTIVITY_NATIVE_UTILITY_MENU_INDEX + 20;

        [MenuItem(SA_Config.EDITOR_PRODUCTIVITY_NATIVE_UTILITY_MENU_ROOT + "Android Manifest/Settings", false, PRIORITY)]
        public static void Setting() {
            AMM_SettingsWindow.ShowTowardsInspector("Manifest", AMM_Skin.SettingsWindowIcon);
        }

        [MenuItem(SA_Config.EDITOR_PRODUCTIVITY_NATIVE_UTILITY_MENU_ROOT + "Android Manifest/Documentation", false, PRIORITY)]
        public static void ISDSetupPluginSetUp() {
            Application.OpenURL("https://unionassets.com/android-manifest-manager");
        }

	}
}
