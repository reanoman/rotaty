﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelGet : MonoBehaviour {
	public string levelName; //start with Level1Icon
	public Sprite[] icons;
	Image iconImage;
	Text iconText; 

	// Use this for initialization
	void Start () {
		iconImage = GetComponent<Image> ();
		iconText = GetComponentInChildren<Text> ();

		if (PlayerPrefs.GetInt (levelName) == 0) {
			iconImage.sprite = icons [0];
			iconText.color = Color.black;
		} else {
			iconImage.sprite = icons [1];
			iconText.color = Color.white;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
