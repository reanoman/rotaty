﻿using UnityEngine;
using System.Collections;

public class TargetAnimation : MonoBehaviour {
	public bool isAnimated, isUp;
	public float speed = 1;
	float yPos;
	public float minRange = -5.72f, maxRange = -4.19f;

	// Use this for initialization
	void Start () {
		yPos = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(isAnimated){
			if(!isUp){
				yPos -= 2 * speed * Time.deltaTime;
			}
			if(transform.position.y < minRange){
				isUp = true;
				yPos += speed * Time.deltaTime;
			}

		}

		transform.position = new Vector3 (transform.position.x, yPos, transform.position.z);
	}
}
