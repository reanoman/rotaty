﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoToLevel : MonoBehaviour {
	public string level;

	public bool isChooseStory;

	public void GoTo(){
		if (isChooseStory) {
			if(PlayerPrefs.GetInt(level) == 1){
				SceneManager.LoadScene (level, LoadSceneMode.Single);
			}
		} else {
            SceneManager.LoadScene(level, LoadSceneMode.Single);
        }
	}
}
