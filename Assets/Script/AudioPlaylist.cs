﻿using UnityEngine;
using System.Collections;

public class AudioPlaylist : MonoBehaviour {
	private static AudioPlaylist instance = null;
	AudioSource audioSource;
	public AudioClip[] musicbg;
	private int i;


	public static AudioPlaylist Instance
	{
		get { return instance; }
	}


	void Awake() {
		audioSource = GetComponent<AudioSource> ();

		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}

	void Start()
	{
		i= Random.Range(0,musicbg.Length);
		StartCoroutine("Playlist");
	}

	IEnumerator Playlist()
	{
		while(true)
		{
			yield return new WaitForSeconds(1.0f);
			if(!audioSource.isPlaying)
			{
				if(i != (musicbg.Length -1))
				{
					i++;
					audioSource.clip = musicbg[i];
					audioSource.Play();
				}
				else
				{
					i=0;
					audioSource.clip= musicbg[i];
					audioSource.Play();
				}
			}
		}
	}
}
