﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
	public bool isWin;
	public string characterTag;
	public TargetCount target;

	public bool isLittleTarget;

	//public Animator targetAnimator;
	public TargetAnimation targetAnimation;

	// Use this for initialization
	void Start () {
		target = FindObjectOfType<TargetCount> ();
		targetAnimation = GetComponent<TargetAnimation> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(!isLittleTarget){
			if(collision.gameObject.tag == characterTag){
				targetAnimation.isAnimated = true;
				//targetAnimator.enabled = true;
				isWin = true;
				target.targets--;
			}
		}
		if (isLittleTarget){
			if(collision.gameObject.tag == characterTag){
				isWin = true;
				target.targets--;
				Destroy (gameObject);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collision){
		if(!isLittleTarget){
			if(collision.gameObject.tag == characterTag){
				targetAnimation.isAnimated = true;
				//targetAnimator.enabled = true;
				isWin = true;
				target.targets--;
			}
		}
		if (isLittleTarget){
			if(collision.gameObject.tag == characterTag){
				isWin = true;
				target.targets--;
				Destroy (gameObject);
			}
		}
	}
}
