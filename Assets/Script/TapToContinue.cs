﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TapToContinue : MonoBehaviour {
    public bool enableClick;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (enableClick) {
            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
                SceneManager.LoadScene("Title", LoadSceneMode.Single);
            }
        }
	}

    public void EnableClick() {
        enableClick = true;
    }
}
