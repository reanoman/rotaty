﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AssignEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Direction direction;

    private void Start()
    {
        direction = Direction.noState;
        AssignButtonFunction();
    }

    void AssignButtonFunction()
    {
        EventTrigger trigger = this.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        EventTrigger.Entry exit = new EventTrigger.Entry();

        entry.eventID = EventTriggerType.PointerDown;
        exit.eventID = EventTriggerType.PointerUp;

        entry.callback.AddListener((data) => { OnPointerDown((PointerEventData)data); });
        exit.callback.AddListener((data) => { OnPointerUp((PointerEventData)data); });

        trigger.triggers.Add(entry);
        trigger.triggers.Add(exit);
    }

    public void OnPointerDown(PointerEventData data)
    {
        //Debug.Log("On Pointer down delegate Called");
        if (this.gameObject.transform.tag == "LeftButton")
        {
            if (Rotate.rotate != null)
            {
                direction = Direction.left;
                Rotate.rotate(direction);
            }
        }
        else if(this.gameObject.transform.tag == "RightButton")
        {
            if (Rotate.rotate != null)
            {
                direction = Direction.right;
                Rotate.rotate(direction);
            }
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        //Debug.Log("On Pointer Up called");
        if (this.gameObject.transform.tag == "LeftButton")
        {
            if (Rotate.rotate != null)
            {
                direction = Direction.noState;
                Rotate.rotate(direction);
            }
        }
        else if (this.gameObject.transform.tag == "RightButton")
        {
            if(Rotate.rotate != null)
            {
                direction = Direction.noState;
                Rotate.rotate(direction);
            }
        }
    }
}
