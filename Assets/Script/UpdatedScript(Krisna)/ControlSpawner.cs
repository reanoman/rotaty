﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ControlSpawner : MonoBehaviour {

    #region Delegate
    public delegate void SpawnPanel();
    public delegate void DestroyPanel();
    #endregion

    #region Delegate Function
    public static SpawnPanel spawnController;
    public static DestroyPanel destroyController;
    #endregion

    public GameObject controller;
    GameObject spawnedController;
    Button leftButton;
    Button rightButton;

    private void OnEnable()
    {
        spawnController += OnStartLevel;
        destroyController += OnEndLevel;

    }

    private void OnDisable()
    {
        spawnController -= OnStartLevel;
        destroyController -= OnEndLevel;
    }

    public void OnStartLevel()
    {
        Debug.Log("Panel Spawned");
        spawnedController = Instantiate(controller);
        RectTransform spawnedPanedRect = spawnedController.GetComponent<RectTransform>();

        spawnedController.transform.SetParent(GameObject.Find("Canvas").transform);
        spawnedController.transform.SetAsFirstSibling();
        spawnedPanedRect.localScale = new Vector2(1, 1);
        spawnedPanedRect.localPosition = new Vector2(0, 0);
        spawnedPanedRect.sizeDelta = new Vector2(900, 1600);

        leftButton = GameObject.FindGameObjectWithTag("LeftButton").GetComponent<Button>();
        rightButton = GameObject.FindGameObjectWithTag("RightButton").GetComponent<Button>();

        return;
    }

    public void OnEndLevel()
    {
        Destroy(spawnedController);
        return;
    }
}
