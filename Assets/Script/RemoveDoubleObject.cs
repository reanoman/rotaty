﻿using UnityEngine;
using System.Collections;

public class RemoveDoubleObject : MonoBehaviour {
	public GameObject[] objects;
	public string objectName;

	void Awake(){
		objects = GameObject.FindGameObjectsWithTag (objectName);

		if(objects.Length == 2){
			Destroy (objects[1]);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
