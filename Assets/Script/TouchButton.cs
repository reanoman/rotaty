﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TouchButton : MonoBehaviour {
	Image buttonImage;
    Text buttonText;
	MainMenu mainMenu;
	public int menuId;
	AudioSource audioSource;
	public AudioClip buttonSound;

    public bool isImageButton;

	// Use this for initialization
	void Start () {
		GameObject audioSourceObject = GameObject.FindGameObjectWithTag ("SFX") as GameObject;
		audioSource = audioSourceObject.GetComponent<AudioSource> ();
        if (!isImageButton)
        {
            buttonText = GetComponent<Text>();
        }
        else {
            buttonImage = GetComponent<Image>();
        }
        
		mainMenu = FindObjectOfType<MainMenu> ();
	}

	void Update(){
		if(mainMenu.mainMenuAnimator.GetInteger("MenuId") == menuId){
            if (!isImageButton)
            {
                buttonText.enabled = true;
            }
            else
            {
                buttonImage.enabled = true;
            }
            
		}
	}

	public void HideButton(){
		audioSource.PlayOneShot (buttonSound);
        if (!isImageButton)
        {
            buttonText.enabled = false;
        }
        else
        {
            buttonImage.enabled = false;
        }        
	}
}
