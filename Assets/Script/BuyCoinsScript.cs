﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class BuyCoinsScript : MonoBehaviour {
	public int poinzzz, poinzTemp;

	Animator buyCoinsAnimator;

	// Use this for initialization
	void Start () {
		buyCoinsAnimator = GetComponent<Animator> ();

		poinzzz = PlayerPrefs.GetInt ("Poinzzz");
	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.SetInt ("Poinzzz", poinzzz);
	}

	public void Point10(){
		poinzTemp = 10;
		buyCoinsAnimator.SetBool ("buyCoins", true);
	}

	public void Point25(){
		poinzTemp = 25;
		buyCoinsAnimator.SetBool ("buyCoins", true);
	}

	public void Point50(){
		poinzTemp = 50;
		buyCoinsAnimator.SetBool ("buyCoins", true);
	}

	public void Point100(){
		poinzTemp = 100;
		buyCoinsAnimator.SetBool ("buyCoins", true);
	}

	public void Point150(){
		poinzTemp = 150;
		buyCoinsAnimator.SetBool ("buyCoins", true);
	}

	public void Point200(){
		poinzTemp = 200;
		buyCoinsAnimator.SetBool ("buyCoins", true);
	}

	public void Back(){
		SceneManager.LoadScene ("Select Story", LoadSceneMode.Single);
	}

	public void Yes(){
		poinzzz += poinzTemp;
		buyCoinsAnimator.SetBool ("buyCoins", false);
	}

	public void No(){
		buyCoinsAnimator.SetBool ("buyCoins", false);
	}
}
