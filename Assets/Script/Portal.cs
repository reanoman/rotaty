﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {
	public Transform portal2;
	public float timeToActive;
	public Portal anotherPortal;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(timeToActive > 0){
			timeToActive -= Time.deltaTime;
			if(timeToActive < 0){
				timeToActive = 0;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collision){
		if(timeToActive == 0){
			if(collision.gameObject.tag == "Grey Character" || collision.gameObject.tag == "Red Character" || collision.gameObject.tag == "Purple Character" || collision.gameObject.tag == "Yellow Character" || collision.gameObject.tag == "Green Character" || collision.gameObject.tag == "Blue Character" || collision.gameObject.tag == "Unknown Character")
            {
				collision.transform.position = portal2.position;
				anotherPortal.timeToActive = 1;
			}
		}
	}
}
