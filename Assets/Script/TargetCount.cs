﻿using UnityEngine;
using System.Collections;
using Colorful;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class TargetCount : MonoBehaviour {

	public int targets, poinzzz, chasingTimePoint, levelBeatInChasingMode;
	public float timeToYes = 3, timeToNext = 2;
	public GameObject yesObject;
	public GameObject[] allObjects;

	public string nextLevel;

	public ContrastVignette contrast;
	public bool decreaseVignette, addPoint, chasingTimeMode;

	public RandomLevel randomLevel;
	public Text timeText;
	public float timeForChasingTime;

    public int levelName;
    string levelNameStr;

    public bool isLastLevel;

    BackToLevel backToLevel;

    //public TimeIsRunningOut timeIsRunning;

    private void OnEnable()
    {

    }


    // Use this for initialization
    void Start () {
        ControlSpawner.spawnController(); //spawn screen control

        levelNameStr = SceneManager.GetActiveScene().name;
        levelNameStr = levelNameStr.Remove(0, 6);

        levelName = int.Parse(levelNameStr);

        if (!isLastLevel)
        {
            nextLevel = "Level " + (levelName + 1);
        }
        else {
            nextLevel = "Select Story";
        }

        backToLevel = FindObjectOfType<BackToLevel>();
        backToLevel.isGameLevel = true;

        if (PlayerPrefs.GetInt ("ChasingTimeMode") == 1){
			chasingTimeMode = true;
			timeText.enabled = true;
			randomLevel = FindObjectOfType<RandomLevel> ();
			timeForChasingTime = Random.Range (7, 15) + PlayerPrefs.GetFloat("TimeForChasingTime");
			levelBeatInChasingMode = PlayerPrefs.GetInt ("LevelBeatInChasingMode");
			chasingTimePoint = PlayerPrefs.GetInt ("ChasingTimePoint");
		}

		poinzzz = PlayerPrefs.GetInt ("Poinzzz");
		//timeIsRunning = Object.FindObjectOfType<TimeIsRunningOut> ();
		contrast = FindObjectOfType<ContrastVignette> ();
		allObjects = GameObject.FindGameObjectsWithTag ("All Objects");
	}
	
	// Update is called once per frame
	void Update () {
		if(chasingTimeMode){
			if(timeForChasingTime > 59){
				timeForChasingTime = 59;
			}
			if(targets > 0){
				timeForChasingTime -= Time.deltaTime;
			}

			if(timeForChasingTime < 10){
				timeText.text = "00:0" + (int)timeForChasingTime;
			}
			else if(timeForChasingTime > 10){
				timeText.text = "00:" + (int)timeForChasingTime;
			}
			 //revisi format detik dan menit
			if(timeForChasingTime < 0){
                SceneManager.LoadScene("Time is Up", LoadSceneMode.Single);
			}
		}

		if(targets == 0){
			if(chasingTimeMode){
				PlayerPrefs.SetFloat ("TimeForChasingTime", timeForChasingTime);
			}

			timeToYes -= Time.deltaTime;
			decreaseVignette = true;
		}

		if(decreaseVignette){
			//if(contrast.Darkness < 0){
				contrast.Darkness -= 7 * Time.deltaTime;
			//}
		}

		if(timeToYes < 0){
			if(!addPoint){
				if(!chasingTimeMode){
					poinzzz++;
					PlayerPrefs.SetInt ("Poinzzz", poinzzz);
				}else if(chasingTimeMode){
					chasingTimePoint++;
					PlayerPrefs.SetInt ("ChasingTimePoint", chasingTimePoint);
				}
				PlayerPrefs.Save ();
				addPoint = true;
			}
			timeToYes = 0;
			//timeIsRunning.DisableText ();
			for(int i=0;i<allObjects.Length;i++){
				allObjects[i].SetActive (false);
			}
			yesObject.SetActive (true);

			timeToNext -= Time.deltaTime;
		}

		if(timeToNext < 0){
			timeToNext = 0;

			if (!chasingTimeMode) {
				int playTime = PlayerPrefs.GetInt("PlayTime");
				playTime++;
				if(playTime > 5){					
					playTime = 0;
					ShowAds ();
				}
				PlayerPrefs.SetInt ("PlayTime", playTime);

				PlayerPrefs.Save ();

                FacebookMainScript.appEventFacebook(FacebookEventType.WinLevelEvent);
                SceneManager.LoadScene(nextLevel, LoadSceneMode.Single);
			} else {				
				levelBeatInChasingMode++;
				PlayerPrefs.SetInt ("LevelBeatInChasingMode", levelBeatInChasingMode);
				randomLevel.LoadRandomLevel ();
			}
		}
	}

	void ShowAds(){
		if(Advertisement.IsReady()){
			Advertisement.Show ();
		}
	}
}
