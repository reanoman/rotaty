﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LockedChapter : MonoBehaviour {
	GoToLevel goToLevel;
	ChooseStory chooseStory;

	// Use this for initialization
	void Start () {
		goToLevel = GetComponentInParent<GoToLevel> ();
		chooseStory = GameObject.FindObjectOfType<ChooseStory> ();

		if (PlayerPrefs.GetInt (goToLevel.level) == 1) {
			Destroy (gameObject);
		} else {	

			goToLevel.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void BuyChapter(){
		if(chooseStory.poinzzz > 50){
			goToLevel.enabled = true;
			PlayerPrefs.SetInt (goToLevel.level, 1);
			chooseStory.poinzzz -= 50;
            chooseStory.SetPoinzzzz();
			Destroy (gameObject);
		}
	}
}
