﻿using UnityEngine;
using System.Collections;

public class DifferentTarget : MonoBehaviour {
	public string differentTag;
	public TargetForCharacter character;

	// Use this for initialization
	void Start () {
		character = GetComponent<TargetForCharacter> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == differentTag){
			character.isGameOver = true;
		}
	}
}
