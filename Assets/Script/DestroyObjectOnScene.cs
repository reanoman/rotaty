﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DestroyObjectOnScene : MonoBehaviour {
	public string level;

	void Awake(){
		if(SceneManager.GetActiveScene().name == level){
			Destroy (gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
