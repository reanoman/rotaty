﻿using UnityEngine;
using System.Collections;

public class RandomRotate : MonoBehaviour {
	public float rotateSpeed, rotateZ;
	public bool enableRotate, rotateLeft;
	public int randomRotateRange;

	public RectTransform rectTransform;

	// Use this for initialization
	void Start () {
		rectTransform = GetComponent<RectTransform> ();
		randomRotateRange = Random.Range (30, 280);
	}
	
	// Update is called once per frame
	void Update () {
		if(enableRotate){
			if (!rotateLeft) {
				transform.Rotate (Vector3.forward, rotateSpeed * Time.deltaTime);
				if (rectTransform.rotation.eulerAngles.z > randomRotateRange) {
					rotateLeft = true;
					randomRotateRange = Random.Range (30, 150);
					rotateSpeed = Random.Range (15, 50);
				}
			} else {
				transform.Rotate (Vector3.forward, -rotateSpeed * Time.deltaTime);
				if (rectTransform.rotation.eulerAngles.z < randomRotateRange) {
					rotateLeft = false;
					randomRotateRange = Random.Range (30, 150);
					rotateSpeed = Random.Range (15, 50);
				}
			}
		}
	}
}
