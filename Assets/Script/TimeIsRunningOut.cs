﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TimeIsRunningOut : MonoBehaviour {
	public TextMesh timeText;
	public float minutes, seconds;
	public bool stopTime;

	public Vector3 objectPosition, objectSize;

	// Use this for initialization
	void Start () {
		timeText = GetComponent<TextMesh> ();

		if(seconds < 10){
			timeText.text = (int)minutes + ":0" + (int)seconds;
		}
		else if(seconds > 10){
			timeText.text = (int)minutes + ":" + (int)seconds;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(SceneManager.GetActiveScene().name == "Score"){
			transform.position = objectPosition;
			transform.localScale = objectSize;
		}

		if(SceneManager.GetActiveScene().name != "Score"){
			if(seconds < 60){
				seconds += Time.deltaTime;
			}
			if(seconds > 60){
				minutes++;
				seconds = 0;
			}
		}

		if(seconds < 10){
			timeText.text = (int)minutes + ":0" + (int)seconds;
		}
		else if(seconds > 10){
			timeText.text = (int)minutes + ":" + (int)seconds;
		}

		if(SceneManager.GetActiveScene().name == "Select Story_PC"){
			Destroy (gameObject);
		}
	}

	public void DisableText(){
		timeText.text = "";
	}
}
