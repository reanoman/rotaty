﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {
	public float minRange, maxRange, speed;
	public bool reverseMove, verticalMove;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!verticalMove) {
			if (!reverseMove) {
				transform.Translate (speed * Time.deltaTime, 0, 0);
				if (transform.localPosition.x > maxRange) {
					transform.localPosition = new Vector3 (maxRange, transform.localPosition.y, transform.localPosition.z);
					reverseMove = true;
				}
			} else {
				transform.Translate (-speed * Time.deltaTime, 0, 0);
				if (transform.localPosition.x < minRange) {
					transform.localPosition = new Vector3 (minRange, transform.localPosition.y, transform.localPosition.z);
					reverseMove = false;
				}
			}
		} 
		else if(verticalMove) {
			if (!reverseMove) {
				transform.Translate (0, speed * Time.deltaTime, 0);
				if (transform.localPosition.y > maxRange) {
					transform.localPosition = new Vector3 (transform.localPosition.x, maxRange, transform.localPosition.z);
					reverseMove = true;
				}
			} else {
				transform.Translate (0, -speed * Time.deltaTime, 0);
				if (transform.localPosition.y < minRange) {
					transform.localPosition = new Vector3 (transform.localPosition.x, minRange, transform.localPosition.z);
					reverseMove = false;
				}
			}
		}
	}
}
