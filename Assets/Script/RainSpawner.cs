﻿using UnityEngine;
using System.Collections;

public class RainSpawner : MonoBehaviour {
	public float timeToSpawn, minTimeToSpawn = 1, maxTimeToSpawn = 5, minRange, maxRange;
	public GameObject[] rain;

	// Use this for initialization
	void Start () {
		timeToSpawn = Random.Range (minTimeToSpawn, maxTimeToSpawn);
	}
	
	// Update is called once per frame
	void Update () {
		timeToSpawn -= Time.deltaTime;
		if(timeToSpawn < 0){
			SpawnRain ();
		}
	}

	void SpawnRain(){
		Instantiate (rain[Random.Range(0, rain.Length)],new Vector3(Random.Range(minRange, maxRange), transform.position.y, transform.position.z), transform.rotation);
		timeToSpawn = Random.Range (minTimeToSpawn, maxTimeToSpawn);
	}
}
