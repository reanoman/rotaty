﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeIsUpScreen : MonoBehaviour {
	public Text getPointText;
	public int chasingTimePoint, poinzzz;

	public Animator timeUpAnimator;

	// Use this for initialization
	void Start () {
		chasingTimePoint = PlayerPrefs.GetInt ("ChasingTimePoint");
		poinzzz = PlayerPrefs.GetInt ("Poinzzz");
	}
	
	// Update is called once per frame
	void Update () {
		getPointText.text = "You beat " + PlayerPrefs.GetInt("LevelBeatInChasingMode") + " levels\nand get " + chasingTimePoint + " point";

		PlayerPrefs.SetInt ("Poinzzz", poinzzz + chasingTimePoint);

		PlayerPrefs.Save ();
	}

	public void Back(){
		PlayerPrefs.SetInt ("ChasingTimePoint", PlayerPrefs.GetInt("ChasingTimePoint") + chasingTimePoint);
		PlayerPrefs.SetInt ("NarratorSound", 0);
		PlayerPrefs.SetInt ("NarratorText", 0);
		PlayerPrefs.SetInt ("ChasingTimeMode", 0);
		timeUpAnimator.SetBool ("isOut", true);
	}

	public void GoToBack(){
        SceneManager.LoadScene("Title", LoadSceneMode.Single);
	}
}
