﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Check every singleton object and assing Dont Destroy state
/// </summary>
public class DontDestroyObject : MonoBehaviour {

	public GameObject[] objects;

	// Use this for initialization
	void Start () {
		for(int i=0;i<objects.Length;i++){
            if (objects[i] == null)
            {
                return;
            }
            else
            {
                DontDestroyOnLoad(objects[i]);
            }
		}
	}
	
}
