﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

using UnityEngine.SceneManagement;

public class TargetForCharacter : MonoBehaviour {
	public GameObject winEffect;
	public string tagTarget;

    public GameObject dieObject;
    //public GameObject dialogueObject;
	public GameObject[] allObjects;

	public bool isGameOver;
	public DecreaseVignette decreaseVignette;

	public AudioSource audioSource;
	public AudioClip beepSound, landingSound;
    public float minPitchSoundRange, maxPitchSoundRange;

	//public TimeIsRunningOut timeIsRunning;

	public float timeToRestart = 2;

	TargetCount targetCount;
	public RestartScript restart;

    public float slimeSoundEffectDelay;

	// Use this for initialization
	void Start () {
		GameObject audioSourceObject = GameObject.FindGameObjectWithTag ("SFX") as GameObject;
		audioSource = audioSourceObject.GetComponent<AudioSource> ();
		//timeIsRunning = Object.FindObjectOfType<TimeIsRunningOut> ();
		allObjects = GameObject.FindGameObjectsWithTag ("All Objects");
		//dialogueObject = GameObject.FindGameObjectWithTag ("Dialogue");

        //dieObject = GameObject.FindGameObjectWithTag("Die Object");

		decreaseVignette = FindObjectOfType<DecreaseVignette> ();

		targetCount = GameObject.FindObjectOfType<TargetCount> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y < -5){
			isGameOver = true;
		}

		if(isGameOver){				
			restart = GameObject.FindGameObjectWithTag ("Game Manager").GetComponent<RestartScript>();
			restart.enableRestart = true;

			timeToRestart -= Time.deltaTime;
			if (targetCount.chasingTimeMode) {
                FacebookMainScript.appEventFacebook(FacebookEventType.TimeUpEvent);
				SceneManager.LoadScene("Time is Up", LoadSceneMode.Single);
			} 

			//timeIsRunning.DisableText ();
			decreaseVignette.decrease = true;

			for(int i=0;i<allObjects.Length;i++){
				allObjects[i].SetActive (false);
			}

			//dialogueObject.SetActive (false);
			dieObject.SetActive (true);
			if(Input.GetButtonDown("Submit")){
                FacebookMainScript.appEventFacebook(FacebookEventType.LoseLevelEvent);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                //Application.LoadLevel (Application.loadedLevel);
            }
		}

        if (slimeSoundEffectDelay > 0) {
            slimeSoundEffectDelay -= Time.deltaTime;
        }
        else {
            slimeSoundEffectDelay = 0;
        }
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.tag == tagTarget) {
			audioSource.PlayOneShot (beepSound);
			Instantiate (winEffect, transform.position, transform.rotation);
			Destroy (gameObject);
		} 

		if(collision.gameObject.tag == "DeadZone"){
			isGameOver = true;
		}

        if (collision.gameObject) {
            if (slimeSoundEffectDelay == 0) {
                audioSource.pitch = Random.Range(minPitchSoundRange, maxPitchSoundRange);
                float vol = Random.Range(.5F, 1);
                audioSource.PlayOneShot(landingSound, vol);
                slimeSoundEffectDelay = .5F;
            }            
        }
	}
}
