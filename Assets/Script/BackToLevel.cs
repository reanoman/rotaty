﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackToLevel : MonoBehaviour {
	public string level;
    public bool isGameLevel;

    //forChecking
    public int levelName;
    string levelNameStr;

    public void Start() {       

        if (isGameLevel) {
            levelNameStr = SceneManager.GetActiveScene().name;
            levelNameStr = levelNameStr.Remove(0, 6);

            levelName = int.Parse(levelNameStr);

            if (levelName >= 1 && levelName <= 15)
            {
                level = "Story 1";
            }
            else if (levelName >= 16 && levelName <= 30)
            {
                level = "Story 2";
            }
            else if (levelName >= 31 && levelName <= 45)
            {
                level = "Story 3";
            }
            else if (levelName >= 46 && levelName <= 60)
            {
                level = "Story 4";
            }
            else if (levelName >= 61 && levelName <= 75)
            {
                level = "Story 5";
            }
            else if (levelName >= 76 && levelName <= 90)
            {
                level = "Story 6";
            }
            else if (levelName >= 91 && levelName <= 105)
            {
                level = "Story 7";
            }
            else if (levelName >= 106 && levelName <= 120)
            {
                level = "Story 8";
            }
            else if (levelName >= 121 && levelName <= 135)
            {
                level = "Story 9";
            }
            else if (levelName >= 136 && levelName <= 150)
            {
                level = "Story 10";
            }
            else if (levelName >= 151 && levelName <= 165)
            {
                level = "Story 11";
            }
            else if (levelName >= 166 && levelName <= 180)
            {
                level = "Story 12";
            }
            else if (levelName >= 181 && levelName <= 195)
            {
                level = "Story 13";
            }
            else if (levelName >= 196 && levelName <= 210)
            {
                level = "Story 14";
            }
            else if (levelName >= 211 && levelName <= 225)
            {
                level = "Story 15";
            }
        }
        
    }

    public void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
            SceneManager.LoadScene(level, LoadSceneMode.Single);
		}
	}

	public void Back(){
		PlayerPrefs.SetInt ("ChasingTimeMode", 0);
        SceneManager.LoadScene(level, LoadSceneMode.Single);
	}
}
