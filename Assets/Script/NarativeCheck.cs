﻿using UnityEngine;
using System.Collections;

public class NarativeCheck : MonoBehaviour {
	public string levels;
	//public AudioSource voice;

	public GameObject back, restart;

	TargetCount targetCount;

	void Awake(){
		//GameObject voiceObject = (GameObject)GameObject.FindGameObjectWithTag ("Voice") as GameObject;
		//voice = voiceObject.GetComponent<AudioSource> ();
		targetCount = GameObject.FindObjectOfType<TargetCount> ();
	}

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetInt(levels) == 0){
			//voice.enabled = true;

			back.SetActive (false);
			restart.SetActive (false);
		}
		else if(PlayerPrefs.GetInt(levels) == 1){
			//voice.enabled = false;

			back.SetActive (true);
			if(!targetCount.chasingTimeMode){
				restart.SetActive (true);
			}
		}

		if(PlayerPrefs.GetInt("NarratorText") == 1 && PlayerPrefs.GetInt("NarratorSound") == 1){
			back.SetActive (true);
			if(!targetCount.chasingTimeMode){
				restart.SetActive (true);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetNarative(){		
		PlayerPrefs.SetInt (levels, 1);
		Debug.Log ("save");
	}

	public void EnableBackAndRestart(){
		back.SetActive (true);
		restart.SetActive (true);
	}
}
