﻿using UnityEngine;
using System.Collections;

public class TutorialLevel1 : MonoBehaviour {
	public GameObject touchRight, touchLeft;
	Touch touch;
	public bool isTouched;

	// Use this for initialization
	void Start () {
		touchLeft.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 1) {
			touch = Input.GetTouch (0);
			if (touch.phase == TouchPhase.Stationary) {
				if (touch.position.x >= Screen.width / 2 && touch.position.x < Screen.width) {
					touchLeft.SetActive (true);
					touchRight.SetActive (false);
					isTouched = true;
				} else if (touch.position.x <= Screen.width / 2 && isTouched) {					
					Destroy (gameObject);
				}                    
			} 
		}
		if(Input.GetAxis("Rotate") > 0){
			touchLeft.SetActive (true);
			touchRight.SetActive (false);
			isTouched = true;
		}
		else if(Input.GetAxis("Rotate") < 0){
			Destroy (gameObject);
		}
	}
}
