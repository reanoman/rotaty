﻿using UnityEngine;
using System.Collections;

public class RainObject : MonoBehaviour {
	public string characterTag;
	public GameObject redEffect;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == characterTag){
			Instantiate (redEffect, transform.position, transform.rotation);
			Destroy (gameObject);
		}
		else if(collision.gameObject.tag != characterTag){
			if (collision.gameObject.GetComponent<TargetForCharacter> ()) {
				collision.gameObject.GetComponent<TargetForCharacter> ().isGameOver = true;
			} else {
				Destroy (gameObject);
			}
		}

		if(collision.gameObject.tag == "DeadZone"){
			Destroy (gameObject);
		}
	}
}
