﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSet : MonoBehaviour {
	public string levelName; //start with Level1Icon
    
    string levelNameStr;

    // Use this for initialization
    void Start () {
        levelNameStr = SceneManager.GetActiveScene().name;
        levelNameStr = levelNameStr.Remove(0, 6);

        levelName = "Level"+levelNameStr+"Icon";

        PlayerPrefs.SetInt (levelName, 1);
	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.Save ();
	}
}
