﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelTitleText : MonoBehaviour {
    Text titleText;

	// Use this for initialization
	void Start () {
        titleText = GetComponent<Text>();
        titleText.text = "" + SceneManager.GetActiveScene().name;
        titleText.CrossFadeAlpha(0, 3, false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
