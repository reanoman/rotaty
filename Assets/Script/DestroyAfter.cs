﻿using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour {
	public float timeToDestroy;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, timeToDestroy);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
