﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public Animator mainMenuAnimator, playAnimator;
	RandomRotate rotate;

	public Sprite[] checkButtons;

	public Image narratorSound, narratorText, bgm, sfx;

	public RandomLevel randomLevel;

    public string levelName;

    public AudioSource BGM, SFX;

    // Use this for initialization
    void Start () {
		mainMenuAnimator = GetComponent<Animator> ();
		rotate = FindObjectOfType<RandomRotate> ();
		randomLevel = FindObjectOfType<RandomLevel> ();

        GameObject bgmObject = GameObject.FindGameObjectWithTag("BGM") as GameObject;
        BGM = bgmObject.GetComponent<AudioSource>();

        GameObject sfxObject = GameObject.FindGameObjectWithTag("SFX") as GameObject;
        SFX = sfxObject.GetComponent<AudioSource>();

        PlayerPrefs.SetInt ("ChasingTimeMode", 0);
		PlayerPrefs.SetInt ("LevelBeatInChasingMode", 0);
		PlayerPrefs.SetFloat ("TimeForChasingTime", 0);
		PlayerPrefs.SetInt ("ChasingTimePoint", 0);

		PlayerPrefs.SetInt ("NarratorSound", 1);
		PlayerPrefs.SetInt ("NarratorText", 1);

		if(PlayerPrefs.GetInt("BGM") == 0){
			bgm.sprite = checkButtons[0];
            BGM.volume = 1;
        }
		else if(PlayerPrefs.GetInt("BGM") == 1){
			bgm.sprite = checkButtons[1];
            BGM.volume = 0;
        }

		if(PlayerPrefs.GetInt("SFX") == 0){
			sfx.sprite = checkButtons[0];
            SFX.volume = 1;
        }
		else if(PlayerPrefs.GetInt("SFX") == 1){
			sfx.sprite = checkButtons[1];
            SFX.volume = 0;
        }

        GoToMainMenu();
	}

	void Update(){

	}

	public void GoToMainMenu(){
		mainMenuAnimator.SetInteger ("MenuId", 1);
	}

	public void GoToPlay(){
		mainMenuAnimator.SetInteger ("MenuId", 2);
	}

	public void BuyPoints(){
		SceneManager.LoadScene("Buy Points", LoadSceneMode.Single);
	}

	public void GoToAdventure(){
		mainMenuAnimator.SetInteger ("MenuId", 3);
	}

	public void GoToChasingTime(){
		mainMenuAnimator.SetInteger ("MenuId", 6);
		PlayerPrefs.SetInt ("NarratorSound", 1);
		PlayerPrefs.SetInt ("NarratorText", 1);
		PlayerPrefs.SetInt ("ChasingTimeMode", 1);
	}

	public void GoToExit(){
		mainMenuAnimator.SetInteger ("MenuId", 4);
	}

	public void GoToSetting(){
		mainMenuAnimator.SetInteger ("MenuId", 5);
	}

	public void Exit(){
		Application.Quit ();
	}

	public void Adventure(){
        SceneManager.LoadScene("Select Story", LoadSceneMode.Single);
	}

	public void ChasingTime(){
		randomLevel.LoadRandomLevel ();
	}

	public void BGMButton(){
		if(PlayerPrefs.GetInt("BGM") == 0){
			PlayerPrefs.SetInt ("BGM", 1);
			bgm.sprite = checkButtons[1];
            BGM.volume = 0;
        }
		else if(PlayerPrefs.GetInt("BGM") == 1){
			PlayerPrefs.SetInt ("BGM", 0);
			bgm.sprite = checkButtons[0];
            BGM.volume = 1;
        }

        PlayerPrefs.Save();
	}

	public void SFXButton(){
		if(PlayerPrefs.GetInt("SFX") == 0){
			sfx.sprite = checkButtons[1];
			PlayerPrefs.SetInt ("SFX", 1);
            SFX.volume = 0;
        }
		else if(PlayerPrefs.GetInt("SFX") == 1){
			sfx.sprite = checkButtons[0];
			PlayerPrefs.SetInt ("SFX", 0);
            SFX.volume = 1;
        }

        PlayerPrefs.Save();
    }

	public void EnableRotateSquare(){
        playAnimator.enabled = true;
		rotate.enableRotate = true;
	}

	public void DisableRotateSquare(){
        playAnimator.enabled = false;
        rotate.transform.rotation = Quaternion.Euler (0, 0, 0);
		rotate.enableRotate = false;
	}
}
