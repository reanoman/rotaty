﻿public enum FacebookEventType
{
    WinLevelEvent, BasicEvent, LoseLevelEvent, TimeUpEvent, Restarted
}