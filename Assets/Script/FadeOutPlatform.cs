﻿using UnityEngine;
using System.Collections;

public class FadeOutPlatform : MonoBehaviour {
	public float timeToGone = 2;
	public bool gone;
	public GameObject goneParticle;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(gone){
			timeToGone -= Time.deltaTime;
			if(timeToGone  < 0){
				Instantiate (goneParticle, transform.position, transform.rotation);
				Destroy (gameObject);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == "Grey Character" || collision.gameObject.tag == "Red Character" || collision.gameObject.tag == "Purple Character" || collision.gameObject.tag == "Yellow Character" || collision.gameObject.tag == "Blue Character" || collision.gameObject.tag == "Pink Character" || collision.gameObject.tag == "Green Character"){
			gone = true;
		}
	}
}
