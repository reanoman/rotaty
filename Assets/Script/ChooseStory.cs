﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

using UnityEngine.SceneManagement;

public class ChooseStory : MonoBehaviour {
	Animator chooseStoryAnimator;
	public string level;
	AudioSource audioSource;
	public AudioClip buttonSound;
    
	public Text pointText;
	public int poinzzz;

	// Use this for initialization
	void Start () {
		poinzzz = PlayerPrefs.GetInt ("Poinzzz");
		GameObject audioSourceObject = GameObject.FindGameObjectWithTag ("SFX") as GameObject;
		audioSource = audioSourceObject.GetComponent<AudioSource> ();
		chooseStoryAnimator = GetComponent<Animator> ();

        pointText.text = "Your Point : " + poinzzz;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GoToNextScene(string gotoLevel){
        SetPoinzzzz();
        audioSource.PlayOneShot (buttonSound);
		chooseStoryAnimator.SetInteger ("MenuId", 1);
		gotoLevel = level;
	}

	public void NextScene(){
        SetPoinzzzz();
        SceneManager.LoadScene(level, LoadSceneMode.Single);
	}

	public void BuyCoins(){
        SetPoinzzzz();
        SceneManager.LoadScene ("Buy Points", LoadSceneMode.Single);
	}

	public void ShowAds(){
		if(Advertisement.IsReady("rewardedVideo")){
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show("rewardedVideo", options);
		}
	}

    public void SetPoinzzzz() {
        PlayerPrefs.SetInt("Poinzzz", poinzzz);
        PlayerPrefs.Save();

        pointText.text = "Your Point : " + poinzzz;
    }

	private void HandleShowResult(ShowResult result){
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
			poinzzz += 5;
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
		}

        SetPoinzzzz();
	}
}
