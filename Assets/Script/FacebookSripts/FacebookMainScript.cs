﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using UnityEngine.SceneManagement;

public class FacebookMainScript : MonoBehaviour {

    static FacebookMainScript instance;

    #region Delegates
    public delegate void InitializeFacebookSDK();
    public delegate void ShareLinkFacebookSDK(string _link);
    public delegate void AppEventFacebook(FacebookEventType _eventType);
    public delegate void LoadNextLevel();
    #endregion

    #region Delegate Static Function
    public static InitializeFacebookSDK initializeFacebook;
    public static ShareLinkFacebookSDK shareLinkFacebook;
    public static AppEventFacebook appEventFacebook;
    public static LoadNextLevel loadNextLevel;
    #endregion

    List<string> perms = new List<string>() { "public_profile", "email" };

    private void OnEnable()
    {
        initializeFacebook += InitCallBack;
        shareLinkFacebook += ShareLink;
        appEventFacebook += CallFacebookEvent;
    }

    private void OnDisable()
    {
        initializeFacebook -= InitCallBack;
        shareLinkFacebook -= ShareLink;
        appEventFacebook -= CallFacebookEvent;
    }

    private void Awake()
    {
        if(!instance)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(this);
        }

        if(!FB.IsInitialized)
        {
            FB.Init(InitCallBack, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }


    /// <summary>
    /// Initialize the FB SDK
    /// </summary>
    void InitCallBack()
    {
        if(FB.IsInitialized)
        {
            Debug.Log("SDK has been launched");
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed Initialize FB SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if(!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    private void AuthCallBack()
    {
        if(FB.IsLoggedIn)
        {
            var aToken = AccessToken.CurrentAccessToken;
            Debug.Log("Access Token");
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
        }
    }

    /// <summary>
    /// Function to share Link
    /// </summary>
    /// <param name="_sharedLink"></param>
    void ShareLink(string _sharedLink)
    {
        Uri uri = new Uri(_sharedLink);
        FB.ShareLink(uri, callback: ShareCallBack); 
    }

    private void ShareCallBack(IShareResult _result)
    {
        if(_result.Cancelled || !string.IsNullOrEmpty(_result.Error))
        {
            Debug.Log("ShareLink Error : " + _result.Error);
        }
        else if(!string.IsNullOrEmpty(_result.PostId))
        {
            Debug.Log(_result.PostId);
        }
        else
        {
            Debug.Log("Share Link Success");
        }
    }

    void CallFacebookEvent(FacebookEventType _eventType)
    {
        Debug.Log("Calling Event");
        var eventParameter = new Dictionary<string, object>();
        switch (_eventType)
        {
            case FacebookEventType.BasicEvent:
                //add something here
                break;
            case FacebookEventType.WinLevelEvent:
                Debug.Log("End level event called");
                eventParameter[AppEventParameterName.ContentID] = "Yes!";
                eventParameter[AppEventParameterName.Description] = "You completed " + SceneManager.GetActiveScene().name;
                eventParameter[AppEventParameterName.Success] = SceneManager.GetActiveScene().name;
                FB.LogAppEvent(AppEventName.AchievedLevel, parameters: eventParameter);
                break;

            case FacebookEventType.LoseLevelEvent:
                Debug.Log("End level event called"); 
                eventParameter[AppEventParameterName.ContentID] = "No!";
                eventParameter[AppEventParameterName.Description] = "You failed at " + SceneManager.GetActiveScene().name;
                eventParameter[AppEventParameterName.Success] = SceneManager.GetActiveScene().name;
                FB.LogAppEvent(AppEventName.AchievedLevel, parameters: eventParameter);
                break;

            case FacebookEventType.TimeUpEvent:
                Debug.Log("End level event called");
                eventParameter[AppEventParameterName.ContentID] = "TimeUp!";
                eventParameter[AppEventParameterName.Description] = "You run out of time at " + SceneManager.GetActiveScene().name;
                eventParameter[AppEventParameterName.Success] = SceneManager.GetActiveScene().name;
                FB.LogAppEvent(AppEventName.AchievedLevel, parameters: eventParameter);
                break;

            case FacebookEventType.Restarted:
                Debug.Log("End level event called");
                eventParameter[AppEventParameterName.ContentID] = "Restarted!";
                eventParameter[AppEventParameterName.Description] = "You restarted " + SceneManager.GetActiveScene().name;
                eventParameter[AppEventParameterName.Success] = SceneManager.GetActiveScene().name;
                FB.LogAppEvent(AppEventName.AchievedLevel, parameters: eventParameter);
                break;
        }
    }
}
