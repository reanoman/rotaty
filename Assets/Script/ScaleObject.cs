﻿using UnityEngine;
using System.Collections;

public class ScaleObject : MonoBehaviour {
	public float scaleMin = 1, scaleMax, scaleSpeed, scaleObj;
	public bool verticalScale, reverseScale;

	// Use this for initialization
	void Start () {
		if (!verticalScale) {
			scaleMax = transform.localScale.x;
		} else {
			scaleMax = transform.localScale.y;
		}

		scaleObj = scaleMax;

	}
	
	// Update is called once per frame
	void Update () {
		if(!verticalScale){
			if (!reverseScale) {
				scaleObj -= scaleSpeed * Time.deltaTime;
				if(transform.localScale.x < scaleMin){
					scaleObj = scaleMin;
					reverseScale = true;
				}
			} else {
				scaleObj += scaleSpeed * Time.deltaTime;
				if(transform.localScale.x > scaleMax){
					scaleObj = scaleMax;
					reverseScale = false;
				}
			}

			transform.localScale = new Vector3 (scaleObj, transform.localScale.y, transform.localScale.z);
		}
		else{
			if (!reverseScale) {
				scaleObj -= scaleSpeed * Time.deltaTime;
				if(transform.localScale.y < scaleMin){
					scaleObj = scaleMin;
					reverseScale = true;
				}
			} else {
				scaleObj += scaleSpeed * Time.deltaTime;
				if(transform.localScale.y > scaleMax){
					scaleObj = scaleMax;
					reverseScale = false;
				}
			}

			transform.localScale = new Vector3 (transform.localScale.x, scaleObj, transform.localScale.z);
		}
	}
}
