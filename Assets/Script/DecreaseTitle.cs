﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DecreaseTitle : MonoBehaviour {
	Text titleText;

	// Use this for initialization
	void Start () {
		titleText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		titleText.CrossFadeAlpha (255, 3, true);
	}
}
