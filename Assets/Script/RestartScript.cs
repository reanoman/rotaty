﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class RestartScript : MonoBehaviour {
	float timeToRestart = 3;
	public bool enableRestart;

	void Update(){
		if(enableRestart){
			timeToRestart -= Time.deltaTime;
			if(timeToRestart < 0){
				int playTime = PlayerPrefs.GetInt("PlayTime");
				playTime++;
				if(playTime > 5){
					playTime = 0;
					ShowAds ();
				}
				PlayerPrefs.SetInt ("PlayTime", playTime);
				PlayerPrefs.Save ();

				RestartScene ();
			}
		}
	}

	public void RestartScene(){
        FacebookMainScript.appEventFacebook(FacebookEventType.Restarted);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}

	void ShowAds(){
		if(Advertisement.IsReady()){
			Advertisement.Show ();
		}
	}
}
