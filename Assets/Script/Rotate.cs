﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    #region Delegate 
    public delegate void RotatePipeWihtButton(Direction dir);
    #endregion

    #region Delegate Function
    public static RotatePipeWihtButton rotate;
    #endregion

    [Header("Rotate Properties")]
    private Touch touch;
	public float rotateZ = 0;
	public float rotateSpeed = 2;
	public float rotateZTemp = 0;
	public bool isTouched = false;
	public float akselerasi = 5;
	public bool gamepadDetected;
	public int gamepadLength;

    Direction rotationDirection;

    private void OnEnable()
    {
        rotate += RotatePipeWithButton;
    }

    private void OnDisable()
    {
        rotate -= RotatePipeWithButton;
    }

    // Use this for initialization
    void Start () {
        rotationDirection = Direction.noState;

		rotateZ = transform.rotation.z;

		gamepadLength = Input.GetJoystickNames().Length;
		if(Input.GetJoystickNames().Length > 0){            
			gamepadDetected = true;
		}
		else {
			gamepadDetected = false;
		}
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (rotationDirection == Direction.left)
        {
            rotateZ += rotateSpeed;
            rotateZTemp = rotateZ;
        }
        else if (rotationDirection == Direction.right)
        {
            rotateZ -= rotateSpeed;
            rotateZTemp = rotateZ;
        }
        else
        {
            rotateZ = rotateZTemp;
        }

        this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotateZ));
    }

    void RotatePipeWithButton(Direction dir)
    {
        rotationDirection = dir;
    }

    void RotateWithTouch()
    {
        if (!gamepadDetected)
        {
            if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Stationary)
                {
                    isTouched = true;
                    if (isTouched)
                    {
                        if (touch.position.x <= Screen.width / 2)
                        {
                            rotateZ += rotateSpeed;
                            rotateZTemp = rotateZ;
                        }
                        else if (touch.position.x >= Screen.width / 2 && touch.position.x < Screen.width)
                        {
                            rotateZ -= rotateSpeed;
                            rotateZTemp = rotateZ;
                        }
                    }
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    isTouched = false;
                    rotateZ = rotateZTemp;
                }

            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rotateZ -= rotateSpeed;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rotateZ += rotateSpeed;
            }

            this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotateZ));
        }

        else
        {
            akselerasi = 2;
            transform.RotateAround(transform.position, -transform.forward, Input.GetAxis("Rotate") * akselerasi);
        }

    }
}
