﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RandomLevel : MonoBehaviour {
	public int maxLevel;
	
	public void LoadRandomLevel(){
        SceneManager.LoadScene("Level " + Random.Range(1, maxLevel), LoadSceneMode.Single);
	}
}
