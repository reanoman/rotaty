﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HideBackButton : MonoBehaviour {
	public Animator dialogAnimator;
	BackToLevel backToLevelScript;
	Button backButton;
	Image backImage;

	public string animationName;

	// Use this for initialization
	void Start () {
		backToLevelScript = GetComponent<BackToLevel> ();
		backButton = GetComponent<Button> ();
		backImage = GetComponent<Image> ();

		dialogAnimator = GameObject.FindGameObjectWithTag ("Dialogue").GetComponent<Animator>();

		backToLevelScript.enabled = false;
		backButton.enabled = false;
		backImage.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(dialogAnimator.GetCurrentAnimatorStateInfo(0).IsName(animationName)){
			
		}
	}
}
