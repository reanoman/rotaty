﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {
	public GameObject blackEffect;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == "DeadZone"){
			Instantiate (blackEffect, transform.position, transform.rotation);
			Destroy (gameObject);
		}
	}
}
