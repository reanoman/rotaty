﻿using UnityEngine;
using System.Collections;
using Colorful;

public class DecreaseVignette : MonoBehaviour {
	public ContrastVignette contrast;
	public bool decrease;

	// Use this for initialization
	void Start () {
		contrast = GetComponent<ContrastVignette> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(decrease && contrast.Darkness < 50){
			contrast.Darkness += 5 * Time.deltaTime;
		}

	}
}
